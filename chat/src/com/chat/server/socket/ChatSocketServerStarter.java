package com.chat.server.socket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import com.chat.ChatPreference;
import com.chat.server.ChatServerStarter;
import com.chat.server.IChatServerEventController;
import com.chat.server.ICreateChatServerStarter;
import com.chat.server.IServerContext;
import com.chat.utils.IEventAtIS;
import com.chat.utils.INewRunnable;

public class ChatSocketServerStarter extends ChatServerStarter
{
	public static final ICreateChatServerStarter CREATOR = new ICreateChatServerStarter()
	{
		@Override
		public ChatServerStarter create(INewRunnable newClient, IChatServerEventController eventController,
				IServerContext context, IEventAtIS eventAtIs)
		{
			return new ChatSocketServerStarter(newClient, eventController, context, eventAtIs);
		}
		
	};
		
	public ChatSocketServerStarter(INewRunnable newRunable, IChatServerEventController eventController, 
			IServerContext context, IEventAtIS eventAtIs)
	{		
		super(newRunable, eventController, context, eventAtIs);
	}	
	
	@Override
	public void run()
	{			
		try 
		{
			ServerSocket server = new ServerSocket(ChatPreference.PORT, ChatPreference.SERVER_BACKLOG);
			try 
			{
				System.out.println("server is started");
		
				while (!Thread.currentThread().isInterrupted())
				{
					try 
					{
						Socket accept = server.accept();				
					
						Runnable runnableClient = new ChatSocketServerRunable(accept, eventController, context, eventAtIs);
						newClient.onNew(runnableClient);
					}
					catch (Throwable e)
					{
						e.printStackTrace();
					}
				}
			}
			finally
			{
				server.close();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	

}
