package com.chat.server.socket;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import com.chat.message.Event;
import com.chat.server.IChatServerEventController;
import com.chat.server.IServerContext;
import com.chat.utils.IEventAtIS;

public class ChatSocketServerRunable implements Runnable
{
	private Socket socket;
	private IChatServerEventController eventController;
	private IServerContext context;
	private IEventAtIS eventAtIs;

	public ChatSocketServerRunable(Socket socket, IChatServerEventController eventController, IServerContext context, 
			IEventAtIS eventAtIs)
	{
		super();
		
		this.socket = socket;
		this.eventController = eventController;
		this.context = context;
		this.eventAtIs = eventAtIs;
	}

	@Override
	public void run() 
	{	
		try 
		{
			InputStream is = socket.getInputStream();
			OutputStream os = socket.getOutputStream();
			
			Event eventFromClient = eventController.answer(eventAtIs.receive(is), context);
			eventAtIs.send(eventFromClient, os);
		}
		catch (Throwable e)
		{
			e.printStackTrace();
		}
		finally
		{
			try 
			{
				socket.close();
			}
			catch (Throwable e)
			{
				
			}
		}
	}

}