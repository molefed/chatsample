package com.chat.server.socket.app;

import com.chat.ChatPreference;
import com.chat.server.AChatServerApp;
import com.chat.server.ChatServerEventController;
import com.chat.server.IServerContext;
import com.chat.server.IServerDB;
import com.chat.server.socket.ChatSocketServerStarter;
import com.chat.utils.EventAtISAsObject;

public class ChatServerApp extends AChatServerApp
{
	public ChatServerApp()
	{
		super(ChatServerEventController.INST,
				new IServerContext()
		{
			private ServerDB DB = new ServerDB(ChatPreference.MAX_MESSAGES);
			
			@Override
			public IServerDB getDb()
			{					
				return DB;
			}
		}, 
		ChatSocketServerStarter.CREATOR, 
		EventAtISAsObject.INST);
	}

	public static void main(String args[])
	{
		new ChatServerApp().run();
	}
	
}
