package com.chat.server.socket.app;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import com.chat.server.IServerDB;
import com.chat.server.UserAlreadyExistDBException;
import com.chat.server.UserNotRegisteredDBException;
import com.chat.utils.MessageRow;

public class ServerDB implements IServerDB
{
	private final int maxMesages;
	private Queue<MessageRow> messages = new LinkedList<MessageRow>();
	private ReadWriteLock locker = new ReentrantReadWriteLock();
	private Map<String, Date> users = new HashMap<String, Date>();
	private Long lastMessageId;

	public ServerDB(int maxMesages)
	{
		if (maxMesages < 1)
			throw new IllegalArgumentException("Max message not bigger 0");
		
		this.maxMesages = maxMesages;
	}

	@Override
	public void addMessage(MessageRow message) throws InterruptedException, UserNotRegisteredDBException
	{
		locker.writeLock().lockInterruptibly();
		try
		{
			if (!users.containsKey(message.getUser()))
				throw new UserNotRegisteredDBException();

			if (messages.size() >= maxMesages)
			{
				messages.poll();
			}

			messages.add(message);
			
			if (lastMessageId == null || lastMessageId < message.getId())
				lastMessageId = message.getId();
		} finally
		{
			locker.writeLock().unlock();
		}
	}
	
	@Override
	public List<MessageRow> getMessages() throws InterruptedException
	{
		return getMessages(null, null);
	}

	@Override
	public List<MessageRow> getMessages(Long lastId, String withoutUser) throws InterruptedException
	{
		locker.readLock().lockInterruptibly();
		try
		{	
			List<MessageRow> result = new ArrayList<MessageRow>();
			if (lastId == null || lastId < 1 || (lastMessageId != null && lastMessageId > lastId))
			{			
				for (MessageRow row : messages)
				{
					if (lastId == null || lastId < 1 || row.getId() > lastId)
					{	
						if (withoutUser == null || !withoutUser.equals(row.getUser()))
							result.add(row);					
					}
				}
			}
			return Collections.unmodifiableList(result);			
		} finally
		{
			locker.readLock().unlock();
		}
	}

	@Override
	public boolean isHasUser(String user) throws InterruptedException
	{
		locker.readLock().lockInterruptibly();
		try
		{
			return users.containsKey(user);
		} finally
		{
			locker.readLock().unlock();
		}
	}

	@Override
	public boolean addIfNotExist(String user) throws InterruptedException
	{
		locker.writeLock().lockInterruptibly();
		try
		{
			if (users.containsKey(user))
				return false;

			users.put(user, new Date());

			return true;
		} finally
		{
			locker.writeLock().unlock();
		}
	}
	
	@Override
	public int getUserCount() throws InterruptedException
	{
		locker.readLock().lockInterruptibly();
		try
		{
			return users.size();
		} finally
		{
			locker.readLock().unlock();
		}
	}

	@Override
	public void changeUser(String oldUser, String newUser)
			throws InterruptedException, UserNotRegisteredDBException, UserAlreadyExistDBException
	{
		locker.writeLock().lockInterruptibly();
		try
		{
			if (!users.containsKey(oldUser))
				throw new UserNotRegisteredDBException();
			if (users.containsValue(newUser))
				throw new UserAlreadyExistDBException();

			users.remove(oldUser);
			users.put(newUser, new Date());

		} finally
		{
			locker.writeLock().unlock();
		}
	}
	
	@Override
	public void ping(String user)
			throws InterruptedException
	{
		locker.writeLock().lockInterruptibly();
		try
		{
			if (users.containsKey(user))
				users.put(user, new Date());
		} finally
		{
			locker.writeLock().unlock();
		}
	}

	@Override
	public void removeOlderUser(long timeInSecond) throws InterruptedException
	{
		locker.writeLock().lockInterruptibly();
		try
		{
			Date now = new Date();
			for (Iterator<Map.Entry<String, Date>> it = users.entrySet().iterator(); it.hasNext();)
			{
				Map.Entry<String, Date> entry = it.next();
				long dif = (now.getTime() - entry.getValue().getTime()) / 1000;
				if (dif > timeInSecond)
				{
					it.remove();
				}
			}
		} finally
		{
			locker.writeLock().unlock();
		}
	}

}
