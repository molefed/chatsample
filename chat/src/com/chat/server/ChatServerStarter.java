package com.chat.server;

import com.chat.utils.IEventAtIS;
import com.chat.utils.INewRunnable;


public abstract class ChatServerStarter implements Runnable
{	
	protected INewRunnable newClient;
	protected IChatServerEventController eventController;
	protected IServerContext context;
	protected IEventAtIS eventAtIs;
	
	public ChatServerStarter(INewRunnable newClient, IChatServerEventController eventController, IServerContext context, 
			IEventAtIS eventAtIs)
	{
		this.newClient = newClient;		
		this.eventController = eventController;
		this.context = context;
		this.eventAtIs = eventAtIs;
	}	
	
	@Override
	public abstract void run();
	
	
}
