package com.chat.server;

import java.util.List;

import com.chat.utils.MessageRow;

public interface IServerDB
{

	public void addMessage(MessageRow message) throws InterruptedException, UserNotRegisteredDBException;
	
	public List<MessageRow> getMessages() throws InterruptedException;
	
	public List<MessageRow> getMessages(Long lastId, String withoutUser) throws InterruptedException;
	
	public boolean isHasUser(String user) throws InterruptedException;

	public boolean addIfNotExist(String user) throws InterruptedException;
	
	public int getUserCount() throws InterruptedException;
	
	public void changeUser(String oldUser, String newUser)
			throws InterruptedException, UserNotRegisteredDBException, UserAlreadyExistDBException;
	
	public void ping(String user)
			throws InterruptedException;
	
	public void removeOlderUser(long timeInSecond) throws InterruptedException;
	
}
