package com.chat.server;

public class UserAlreadyExistDBException extends Exception
{
	private static final long serialVersionUID = -6132214010046367518L;
	
	public UserAlreadyExistDBException()
	{
		super("User already exist");
	}

}
