package com.chat.server;

import com.chat.message.Event;

public interface IChatServerEventController
{
	public Event answer(Event event, IServerContext context);

}
