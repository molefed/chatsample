package com.chat.server;

import com.chat.utils.IEventAtIS;
import com.chat.utils.INewRunnable;

public interface ICreateChatServerStarter
{
	public ChatServerStarter create(INewRunnable newRunable, IChatServerEventController eventController,
			IServerContext context, IEventAtIS eventAtIs);
}
