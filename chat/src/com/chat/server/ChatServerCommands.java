package com.chat.server;

import java.util.HashMap;
import java.util.Map;

import com.chat.message.Event;
import com.chat.message.EventCode;

public class ChatServerCommands
{
	public static final ChatServerCommands INST = new ChatServerCommands();
	
	private Map<String, ICommand> map = new HashMap<String, ICommand>();	
	
	public ChatServerCommands()
	{
		init();		
	}	

	public Event executeCommand(String[] params, IServerContext context, String user) throws Throwable
	{
		if (params != null && params.length > 0)
		{
			ICommand cmd = map.get(params[0]);
			if (cmd != null)
			{
				Event event = cmd.execute(params, context, user);
				if (event != null)
					return event;
			}
			else
			{
				throw new ChatServerCommandExeption("Command " + params[0] + " not found");
			}
		}
	
		throw new ChatServerCommandExeption("Command not found");
	}
	
	private interface ICommand
	{
		public Event execute(String[] params, IServerContext context, String user) throws Throwable;
		
		public String getHelp();
	}

	protected void init()
	{
		map.put("help", new ICommand()
		{
			@Override
			public Event execute(String[] params, IServerContext context, String user) 
			{
				StringBuilder sb = new StringBuilder();
				for (ICommand cmd : map.values())
				{
					sb.append(cmd.getHelp());
					sb.append("\n");
				}
				
				return new Event(EventCode.ACCEPTED, sb.toString());
			}
			
			@Override
			public String getHelp()
			{
				return "-help =Help";
			}
			
		});
		
		map.put("change", new ICommand()
		{
			@Override
			public Event execute(String[] params, IServerContext context, String user) throws Throwable
			{
				if (params.length < 2)
					throwParams(params[0]);
				
				context.getDb().changeUser(user, params[1]);				
				return new Event(EventCode.CHANGE_USERNAME, null, params[1]);
			}
			
			@Override
			public String getHelp()
			{
				return "-change newUserName =Change user name";
			}
			
		});
		
		map.put("count", new ICommand()
		{
			@Override
			public Event execute(String[] params, IServerContext context, String user) throws Throwable
			{								
				return new Event(EventCode.ACCEPTED, String.valueOf("User count " + context.getDb().getUserCount()));
			}
			
			@Override
			public String getHelp()
			{
				return "-count =Show user count";
			}
			
		});
		
	}
	
	private void throwParams(String commandName) throws ChatServerCommandExeption
	{
		throw new ChatServerCommandExeption("Command " + commandName + " params not found");
	}
	
	
}
