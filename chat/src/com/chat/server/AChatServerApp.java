package com.chat.server;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.chat.ChatPreference;
import com.chat.utils.IEventAtIS;
import com.chat.utils.INewRunnable;

public class AChatServerApp implements Runnable
{
	private IChatServerEventController eventController;
	private IServerContext context;
	private ICreateChatServerStarter serverCreator; 
	private IEventAtIS eventAtIs;
	private ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
	private ExecutorService executor = Executors.newFixedThreadPool(ChatPreference.SERVER_THREAD_COUNT);
	private INewRunnable newRunable = new INewRunnable()
	{
		@Override
		public void onNew(Runnable runnable)
		{
			executor.submit(runnable);
		}
		
	};

	public AChatServerApp(IChatServerEventController eventController, IServerContext context, 
			ICreateChatServerStarter serverCreator, IEventAtIS eventAtIs)
	{
		this.eventController = eventController;
		this.context= context; 
		this.serverCreator = serverCreator;
		this.eventAtIs = eventAtIs;
	}	
	
	@Override
	public void run()
	{	
		try 
		{		
			scheduledExecutorService.scheduleWithFixedDelay(new Runnable()
			{
	
				@Override
				public void run()
				{
					try 
					{
						context.getDb().removeOlderUser(ChatPreference.ALIVE_USER_TIME_IN_SEC);
					}
					catch (InterruptedException e)
					{
						return;
					}
				}
			}, 0, ChatPreference.TIME_TO_CHEK_ALIVE_USER_IN_SEC, TimeUnit.SECONDS);
				
		
			ChatServerStarter chatServerStarter = serverCreator.create(newRunable, eventController, context, eventAtIs);
			
			chatServerStarter.run();
		} catch (Throwable e)
		{
			e.printStackTrace();
		} finally
		{
			executor.shutdown();
			scheduledExecutorService.shutdown();
		}
	}
}
