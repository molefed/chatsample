package com.chat.server;

public class ChatServerCommandExeption extends Exception
{
	private static final long serialVersionUID = 6599091878696015491L;

	public ChatServerCommandExeption(String message)
	{
		super(message);		
	}
	
}
