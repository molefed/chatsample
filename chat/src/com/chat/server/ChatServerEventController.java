package com.chat.server;

import java.io.Serializable;

import com.chat.message.Event;
import com.chat.message.EventCode;
import com.chat.utils.MessageRow;

public class ChatServerEventController implements IChatServerEventController
{
	public static final ChatServerEventController INST = new ChatServerEventController();
	
	@Override
	public Event answer(Event event, IServerContext context)
	{
		try 
		{
			if (event.getCode() == EventCode.LOGIN)
			{
				if (event.getUser() == null || !context.getDb().addIfNotExist(event.getUser()))
					return new Event(EventCode.REJECTED);
				else
					return new Event(EventCode.ACCEPTED, (Serializable) context.getDb().getMessages(), event.getUser());
			} else if (event.getUser() == null || !context.getDb().isHasUser(event.getUser()))
			{
				return new Event(EventCode.NEED_LOGIN);
			} else
			{
				switch (event.getCode())
				{
				case EventCode.MESSAGE:
					return command(context, event.getUser(), (String) event.getAny());
	
				case EventCode.GET_MESSAGES:
					context.getDb().ping(event.getUser());				
					return new Event(EventCode.ACCEPTED,
							(Serializable) context.getDb().getMessages((Long) event.getAny(), event.getUser()));
				}
			}
		}
		catch (Throwable e)
		{
			e.printStackTrace();
			
			return new Event(EventCode.REJECTED, e.getMessage());
		}
		
		return new Event(EventCode.REJECTED);
	}
	
	private Event command(IServerContext context, String user, String message) throws Throwable
	{
		if (message == null || !message.startsWith("-"))
		{
			MessageRow row = new MessageRow(user, message);
			context.getDb().addMessage(row);
			return new Event(EventCode.ACCEPTED, row.getId());
		}
		else if (message.length() > 1)
		{
			return ChatServerCommands.INST.executeCommand(message.substring(1).split(" "), 
					context, user);				
		}		
		
		return new Event(EventCode.REJECTED, "Message incorrect");
	}


}