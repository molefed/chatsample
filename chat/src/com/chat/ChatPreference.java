package com.chat;


public class ChatPreference
{
	public static final int PORT = 61754;
	public static final String SERVER_IP = "127.0.0.1";
	public static final int MAX_MESSAGES = 100;	
	public static final int SERVER_BACKLOG = 100000;
	public static final int SERVER_THREAD_COUNT = 20;
	public static final int CLIENT_TIMEOUT_IN_SEC = 10;
	public static final int ALIVE_USER_TIME_IN_SEC = 30;
	public static final int TIME_TO_CHEK_ALIVE_USER_IN_SEC = 10;
	public static final int TIME_TO_SEND_PING_ALIVE_USER_IN_SEC = 5;
	

}
