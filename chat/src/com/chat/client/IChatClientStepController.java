package com.chat.client;


public interface IChatClientStepController
{
	
	public void step(IClientContext context) throws Throwable;

}
