package com.chat.client;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.chat.ChatPreference;
import com.chat.message.Event;
import com.chat.message.EventCode;
import com.chat.utils.MessageRow;


public class AChatClientApp implements Runnable 
{
	private IClientContext context;
	private IChatClientStepController stepController;
	
	public AChatClientApp(IClientContext context, IChatClientStepController stepController)
	{
		this.context = context;
		this.stepController = stepController;
	}

	@Override
	public void run()
	{
		ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);		
		scheduledExecutorService.scheduleWithFixedDelay(new Runnable()
		{

			@Override
			public void run()
			{
				try 
				{
					if (context.getUser() == null)
						return;
	
					Event fromServer = context.send(new Event(EventCode.GET_MESSAGES, 
							context.getLastId(), context.getUser()));	
					
					@SuppressWarnings("unchecked")
					List<MessageRow> rows = (List<MessageRow>) fromServer.getAny();
					context.printLastMessage(rows);
				}
				catch (Throwable e)
				{
					e.printStackTrace();					
				}
			}
		}, 0, ChatPreference.TIME_TO_SEND_PING_ALIVE_USER_IN_SEC, TimeUnit.SECONDS);
		
		while (!Thread.currentThread().isInterrupted())
		{
			try 
			{
				stepController.step(context);
			}
			catch (Throwable e)
			{
				e.printStackTrace();
			}
		}
		
		scheduledExecutorService.shutdown();
	}

}