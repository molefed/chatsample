package com.chat.client;

import java.util.List;

import com.chat.message.Event;
import com.chat.message.EventCode;
import com.chat.utils.MessageRow;

public class ChatClientStepController implements IChatClientStepController
{
	public static final ChatClientStepController INST = new ChatClientStepController();
	
	@Override
	public void step(IClientContext context) throws Throwable
	{
		if (context.getUser() == null)
		{
			login(context);
			
			return;
		}
		
		String line = context.nextLine();
		context.println("");
		
		Event fromServer =  context.send(new Event(EventCode.MESSAGE, line, context.getUser()));

		if (fromServer.getCode() == EventCode.NEED_LOGIN)
		{
			context.setUser(null);
		}		
		else if (fromServer.getCode() == EventCode.CHANGE_USERNAME)
		{
			context.setUser(fromServer.getUser());
			context.println("User name changed.");
		}
		else
		{
			if (fromServer.getAny() != null && fromServer.getAny() instanceof String)
			{
				String message = (String) fromServer.getAny();				
				context.println(message);
			}
		}
	}
	
	private void login(IClientContext context) throws Throwable
	{
		context.println("Input user name to login:");
		String line = context.nextLine();
		context.println("");
	
		Event fromServer = context.send(new Event(EventCode.LOGIN, null, line));
		
		if (fromServer.getCode() == EventCode.ACCEPTED)
		{
			context.println("You was logged.");
			
			context.setUser(fromServer.getUser());
			
			@SuppressWarnings("unchecked")
			List<MessageRow> rows = (List<MessageRow>) fromServer.getAny();
			context.printLastMessage(rows);
		}
		else
		{
			context.println("Incorrect user name. Try again.");
			context.setUser(null);
		}
	}
	
}
