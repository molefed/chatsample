package com.chat.client;

import java.util.List;

import com.chat.utils.MessageRow;

public abstract class AClientContext implements IClientContext
{
	
	@Override
	public void printLastMessage(List<MessageRow> rows)
	{
		if (rows == null)
			return;
		
		for (MessageRow row : rows)
		{
			println(row.getUser() + ":" + row.getMessage());
			setIdIfBigger(row.getId());
		}		
		
	}
	
}
