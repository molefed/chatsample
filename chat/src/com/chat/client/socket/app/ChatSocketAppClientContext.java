package com.chat.client.socket.app;

import java.util.Scanner;

import com.chat.client.socket.AChatSocketClientContext;
import com.chat.utils.IEventAtIS;
import com.chat.utils.LastIdMessage;

public class ChatSocketAppClientContext extends AChatSocketClientContext
{
	private Scanner keyboard = new Scanner(System.in);
	private volatile String user;
	private LastIdMessage lastIdMessage = new LastIdMessage();
		
	public ChatSocketAppClientContext(IEventAtIS eventAtIs)
	{
		super(eventAtIs);
	}
	
	@Override
	public String getUser()
	{
		return user;
	}

	@Override
	public void setUser(String user)
	{
		this.user = user;
	}

	@Override
	public String nextLine()
	{		
		return keyboard.nextLine();
	}

	@Override
	public void println(String s)
	{
		System.out.println(s);
	}

	@Override
	public void setIdIfBigger(Long id)
	{
		lastIdMessage.setIdIfBigger(id);		
	}
	
	@Override
	public Long getLastId()
	{
		return lastIdMessage.getId();		
	}
	
}
