package com.chat.client.socket.app;

import com.chat.client.AChatClientApp;
import com.chat.client.ChatClientStepController;
import com.chat.utils.EventAtISAsObject;


public class ChatClientApp extends AChatClientApp
{
	
	public ChatClientApp()
	{
		super(new ChatSocketAppClientContext(EventAtISAsObject.INST), ChatClientStepController.INST);
	}

	public static void main(String[] arg)
	{
		new ChatClientApp().run();
	}
	

}