package com.chat.client.socket;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

import com.chat.ChatPreference;
import com.chat.client.AClientContext;
import com.chat.message.Event;
import com.chat.utils.IEventAtIS;

public abstract class AChatSocketClientContext extends AClientContext
{
	private IEventAtIS eventAtIs;

	public AChatSocketClientContext(IEventAtIS eventAtIs)
	{
		this.eventAtIs = eventAtIs;
	}

	@Override
	public Event send(Event event) throws Throwable
	{
		Socket socket = new Socket();		
		try
		{
			socket.connect(new InetSocketAddress(InetAddress.getByName(ChatPreference.SERVER_IP) , ChatPreference.PORT), 
					ChatPreference.CLIENT_TIMEOUT_IN_SEC * 1000); 
			
			OutputStream os = socket.getOutputStream();
			InputStream is = socket.getInputStream();
			eventAtIs.send(event, os);
			return eventAtIs.receive(is);
		} finally
		{
			socket.close();
		}
	}

}
