package com.chat.client;

import java.util.List;

import com.chat.message.Event;
import com.chat.utils.MessageRow;

public interface IClientContext
{
	public String getUser();
	
	public void setUser(String user);

	public Event send(Event event) throws Throwable;
	
	public String nextLine();
	
	public void println(String s);
	
	public void printLastMessage(List<MessageRow> rows);	
	
	public void setIdIfBigger(Long id);	
	
	public Long getLastId();
}
