package com.chat.utils;

public interface INewRunnable
{
	
	public void onNew(Runnable runnable);

}
