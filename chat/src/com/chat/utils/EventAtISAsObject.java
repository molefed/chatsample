package com.chat.utils;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import com.chat.message.Event;

public class EventAtISAsObject implements IEventAtIS
{
	public static final EventAtISAsObject INST = new EventAtISAsObject(); 
	
	@Override
	public void send(Event m, OutputStream os) throws Throwable
	{
		ObjectOutputStream os1 = new ObjectOutputStream(os);
		os1.writeObject(m);
	}
	
	@Override
	public Event receive(InputStream is) throws Throwable
	{
		ObjectInputStream is1 = new ObjectInputStream(is);
		return (Event) is1.readObject();
	}

}
