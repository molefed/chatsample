package com.chat.utils;

public class LastIdMessage
{
	private Long id;
	
	public LastIdMessage()
	{
		
	}

	public synchronized Long getId()
	{
		return id;
	}

	public synchronized void setIdIfBigger(Long id)
	{
		if (this.id == null || (id != null && id > this.id))
			this.id = id;
	}
	
	public synchronized void clear()
	{
		this.id = null;
	}

}
