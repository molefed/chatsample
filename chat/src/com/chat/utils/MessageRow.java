package com.chat.utils;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicLong;

public class MessageRow implements Serializable
{	
	private static final long serialVersionUID = -1074068791963822861L;
	private static final AtomicLong COUNTER = new AtomicLong(1);
	
	private long id;
	private String user;
	private String message;
	
	protected MessageRow()
	{
		
	}
	
	public MessageRow(String user, String message)
	{
		this.id = COUNTER.incrementAndGet();
		this.user = user;
		this.message = message;
	}
	
	public long getId()
	{
		return id;
	}

	public String getUser()
	{
		return user;
	}

	public void setUser(String user)
	{
		this.user = user;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

}
