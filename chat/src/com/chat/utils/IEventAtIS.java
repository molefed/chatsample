package com.chat.utils;

import java.io.InputStream;
import java.io.OutputStream;

import com.chat.message.Event;

public interface IEventAtIS
{
	
	public void send(Event m, OutputStream os) throws Throwable;
	
	public Event receive(InputStream is) throws Throwable;

}
