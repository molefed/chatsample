package com.chat.message;

import java.io.Serializable;
import java.util.Date;

public class Event implements Serializable
{
	private static final long serialVersionUID = 4536562670042979812L;
	
	private int code;
	private String user;
	private Date dateSend = new Date();
	private Serializable any;
	
	protected Event()
	{
		
	}
	
	public Event(int code)
	{
		this();
		
		this.code = code;
	}
	
	public Event(int code, Serializable any)
	{
		this(code);
		
		this.any = any;
	}
	
	public Event(int code, Serializable any, String user)
	{
		this(code, any);
		
		this.user = user;
	}
	
	public int getCode()
	{
		return code;
	}

	public void setCode(int code)
	{
		this.code = code;
	}

	public String getUser()
	{
		return user;
	}

	public void setUser(String user)
	{
		this.user = user;
	}

	public Date getDateSend()
	{
		return dateSend;
	}

	public void setDateSend(Date dateSend)
	{
		this.dateSend = dateSend;
	}

	public Serializable getAny()
	{
		return any;
	}

	public void setAny(Serializable any)
	{
		this.any = any;
	}

	
	
	
	

}
