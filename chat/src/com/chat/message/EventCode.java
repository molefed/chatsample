package com.chat.message;

public class EventCode
{
	public static final int MESSAGE = 1;
	public static final int ACCEPTED = 2;
	public static final int REJECTED = 3;
	public static final int LOGIN = 4;
	public static final int NEED_LOGIN = 5;	
	public static final int GET_MESSAGES = 6;
	public static final int CHANGE_USERNAME = 7;
}
