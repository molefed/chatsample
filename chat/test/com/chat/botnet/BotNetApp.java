package com.chat.botnet;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.chat.client.AChatClientApp;
import com.chat.client.ChatClientStepController;
import com.chat.client.socket.app.ChatSocketAppClientContext;
import com.chat.server.socket.app.ChatServerApp;
import com.chat.utils.EventAtISAsObject;

public class BotNetApp
{

	public static void main(String args[])
	{
		Thread serverThread = new Thread(new ChatServerApp());
		serverThread.start();
		
		final Random rnd = new Random();
		int countClients = 1000;		
		final ExecutorService executor = Executors.newFixedThreadPool(countClients);
		for (int i = 0; i < countClients; i++)
		{
			final int number = i; 
			executor.submit(new AChatClientApp(
					new ChatSocketAppClientContext(EventAtISAsObject.INST)
					{
						private boolean isLogin = true;
						
						@Override
						public String nextLine()
						{		
							try
							{
								Thread.sleep((1 + rnd.nextInt(60)) * 1000);
							}
							catch (InterruptedException e)
							{
								return null;
							}
							
							String s = null;
							if (isLogin)
							{
								s = "user" + number;
								isLogin = false;
							}
							else
							{
								s = "fromBot" + rnd.nextInt();
							}
							
							println(s);
							return s;
						}
												
					}, ChatClientStepController.INST));
		}
	
	}
	
}
