package com.chat.botnet;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TestExecutor
{
	
	public static void main(String args[])
	{
		ExecutorService executor = Executors.newFixedThreadPool(1);
		
		for (int i=0; i < 10; i++)
		{
			final int num = i;
			
			executor.submit(new Runnable()
			{
				
				@Override
				public void run()
				{					
					System.out.println(num);
					
					try 
					{
						Thread.sleep(1000);
					}
					catch (InterruptedException e)
					{
						return;
					}
				}
			});
		}
		
		executor.shutdown();
	}
	

}
