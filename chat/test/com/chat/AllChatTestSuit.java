package com.chat;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.chat.client.ChatClientStepControllerTest;
import com.chat.server.ChatServerEventControllerTest;
import com.chat.server.socket.app.ServerDBTest;
import com.chat.utils.UtilsTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	UtilsTest.class,
	ServerDBTest.class,
	ChatServerEventControllerTest.class,
	ChatClientStepControllerTest.class
})
public class AllChatTestSuit
{

}
