package com.chat.client;

import com.chat.message.Event;
import com.chat.server.IChatServerEventController;
import com.chat.server.IServerContext;
import com.chat.server.IServerDB;
import com.chat.server.socket.app.ServerDB;
import com.chat.utils.LastIdMessage;

public abstract class ATestClientContext extends AClientContext
{
	private String user;
	private ServerDB DB;			
	private IServerContext context = new IServerContext()
	{
		@Override
		public IServerDB getDb()
		{					
			return DB;
		}
	};
	private LastIdMessage lastIdMessage = new LastIdMessage();
	private IChatServerEventController controller;
	
	public ATestClientContext(IChatServerEventController controller, ServerDB DB)
	{
		this.controller = controller;
		this.DB = DB;
	}
	

	@Override
	public String getUser()
	{
		return user;
	}

	@Override
	public void setUser(String user)
	{
		this.user = user;				
	}

	@Override
	public Event send(Event event) throws Throwable
	{				
		return controller.answer(event, context);
	}

	@Override
	public void setIdIfBigger(Long id)
	{
		lastIdMessage.setIdIfBigger(id);				
	}

	@Override
	public Long getLastId()
	{
		return lastIdMessage.getId();
	}

}