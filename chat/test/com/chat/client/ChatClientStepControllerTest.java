package com.chat.client;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.chat.ChatPreference;
import com.chat.server.ChatServerEventController;
import com.chat.server.socket.app.ServerDB;

public class ChatClientStepControllerTest
{	
	private String nextLine;
	private List<String> printLn = new ArrayList<String>(); 
	private ServerDB DB = new ServerDB(ChatPreference.MAX_MESSAGES);

	protected IChatClientStepController createStepController()
	{
		return ChatClientStepController.INST;
	}
	
	private String getLastOutput()
	{
		return printLn.get(printLn.size() - 1);
	}
	
	private class TestClientContext extends ATestClientContext
	{
		
		public TestClientContext()
		{
			super(ChatServerEventController.INST, DB);			
		}
		
		@Override
		public String nextLine()
		{				
			return nextLine;
		}

		@Override
		public void println(String s)
		{
			printLn.add(s);
		}

	};

	@Test
	public void testStep() throws Throwable
	{
		IChatClientStepController stepController = createStepController();
		IClientContext context1 = new TestClientContext();
		
		nextLine = "user1";
		stepController.step(context1);
		assertEquals(getLastOutput(), "You was logged.");
		
		nextLine = "hi user2";
		stepController.step(context1);
		
		IClientContext context2 = new TestClientContext();
		nextLine = "user1";
		stepController.step(context2);
		assertEquals(getLastOutput(), "Incorrect user name. Try again.");
		
		nextLine = "user2";
		stepController.step(context2);
		assertEquals(getLastOutput(), "user1:" + "hi user2");
		
		nextLine = "-change user22";
		stepController.step(context2);
		assertEquals(getLastOutput(), "User name changed.");
		
		assertEquals(DB.getUserCount(), 2);
				
		
//		System.out.println(printLn);
		
		
	}

}
