package com.chat.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.chat.message.Event;
import com.chat.message.EventCode;

public class UtilsTest
{

	@Test
	public void testLastIdMessage()
	{
		LastIdMessage lastIdMessage = new LastIdMessage();

		lastIdMessage.setIdIfBigger(1L);
		assertEquals(lastIdMessage.getId().longValue(), 1);

		lastIdMessage.setIdIfBigger(10L);
		assertEquals(lastIdMessage.getId().longValue(), 10L);

		lastIdMessage.setIdIfBigger(5L);
		assertEquals(lastIdMessage.getId().longValue(), 10L);

		lastIdMessage.clear();
		assertNull(lastIdMessage.getId());
	}

	@Test
	public void testMessageRow()
	{
		Long lastId = null;
		for (int i = 0; i < 10; i++)
		{
			MessageRow row = new MessageRow("", "");
			if (lastId != null)
				assertTrue(lastId < row.getId());

			lastId = row.getId();
		}
	}

	@Test
	public void testEventAtISAsObject() throws Throwable
	{
		IEventAtIS eventAtIS = EventAtISAsObject.INST;
		Event eventSource = new Event(EventCode.ACCEPTED, (Serializable) Arrays.asList("a", "b", "c"), "user1");
		
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		eventAtIS.send(eventSource, os);
		
		InputStream is = new ByteArrayInputStream(os.toByteArray());
		Event eventReceived = eventAtIS.receive(is);
		
		assertEquals(eventSource.getCode(), eventReceived.getCode());
		assertEquals(eventSource.getUser(), eventReceived.getUser());
		
		@SuppressWarnings("unchecked")
		List<String> list = (List<String>) eventReceived.getAny();
		assertNotNull(list);
		assertEquals(list.size(), 3);
		assertEquals(list.get(0), "a");
		assertEquals(list.get(1), "b");
		assertEquals(list.get(2), "c");
		
	}

}
