package com.chat.server;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import com.chat.ChatPreference;
import com.chat.message.Event;
import com.chat.message.EventCode;
import com.chat.server.socket.app.ServerDB;
import com.chat.utils.MessageRow;

public class ChatServerEventControllerTest
{
	
	protected IChatServerEventController getController()
	{
		return ChatServerEventController.INST;
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testController()
	{
		IChatServerEventController controller = getController();
		
		IServerContext context = new IServerContext()
		{
			private ServerDB DB = new ServerDB(ChatPreference.MAX_MESSAGES);
			
			@Override
			public IServerDB getDb()
			{					
				return DB;
			}
		};
		
		Event fromServer = controller.answer(new Event(EventCode.MESSAGE, "message1", "user1"), context);
		assertEquals(fromServer.getCode(), EventCode.NEED_LOGIN);
		
		fromServer = controller.answer(new Event(EventCode.LOGIN, null, "user1"), context);
		assertEquals(fromServer.getCode(), EventCode.ACCEPTED);
		
		fromServer = controller.answer(new Event(EventCode.LOGIN, null, "user1"), context);
		assertEquals(fromServer.getCode(), EventCode.REJECTED);
		
		fromServer = controller.answer(new Event(EventCode.MESSAGE, "message1", "user1"), context);
		assertEquals(fromServer.getCode(), EventCode.ACCEPTED);
		
		fromServer = controller.answer(new Event(EventCode.MESSAGE, "-help", "user1"), context);
		assertEquals(fromServer.getCode(), EventCode.ACCEPTED);
		assertTrue(((String) fromServer.getAny()).toLowerCase().contains("help"));
		
		fromServer = controller.answer(new Event(EventCode.MESSAGE, "-count", "user1"), context);
		assertEquals(fromServer.getCode(), EventCode.ACCEPTED);
		assertEquals((String) fromServer.getAny(), "User count " + "1");
		
		fromServer = controller.answer(new Event(EventCode.GET_MESSAGES, null, "user1"), context);
		assertEquals(fromServer.getCode(), EventCode.ACCEPTED);
		assertEquals(((List<MessageRow>) fromServer.getAny()).size(), 0);
		
		fromServer = controller.answer(new Event(EventCode.MESSAGE, "-change user11", "user1"), context);
		assertEquals(fromServer.getCode(), EventCode.CHANGE_USERNAME);
		
		fromServer = controller.answer(new Event(EventCode.MESSAGE, "message1", "user1"), context);
		assertEquals(fromServer.getCode(), EventCode.NEED_LOGIN);
		
		fromServer = controller.answer(new Event(EventCode.MESSAGE, "message1", "user11"), context);
		assertEquals(fromServer.getCode(), EventCode.ACCEPTED);
		
		fromServer = controller.answer(new Event(EventCode.GET_MESSAGES, null, "user11"), context);
		assertEquals(fromServer.getCode(), EventCode.ACCEPTED);
		assertEquals(((List<MessageRow>) fromServer.getAny()).size(), 1);
				
		
	}

}
