package com.chat.server.socket.app;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import com.chat.server.IServerDB;
import com.chat.utils.MessageRow;

public class ServerDBTest
{
	private static final int MAX_MESAGES = 10;

	protected IServerDB createDB(int maxMessage)
	{
		return new ServerDB(maxMessage);
	}

	@Test
	public void testDB() throws Throwable
	{
		IServerDB db = createDB(MAX_MESAGES);
		db.addIfNotExist("user1");
		for (int i = 0; i < MAX_MESAGES; i++)
		{
			db.addMessage(new MessageRow("user1", "message" + i));
		}
		assertEquals(db.getMessages().size(), MAX_MESAGES);

		for (int addIndex = 0; addIndex < 5; addIndex++)
		{
			List<MessageRow> rows = db.getMessages();
			assertEquals(rows.size(), MAX_MESAGES);

			for (int i = 0; i < rows.size(); i++)
			{
				MessageRow row = rows.get(i);
				assertEquals(row.getUser(), "user1");
				assertEquals(row.getMessage(), "message" + (i + addIndex));
			}

			db.addMessage(new MessageRow("user1", "message" + (MAX_MESAGES + addIndex)));
		}

		List<MessageRow> rows = db.getMessages(null, "user1");
		assertEquals(rows.size(), 0);

		rows = db.getMessages();
		Long idAfterLast = rows.get(rows.size() - 2).getId();
		rows = db.getMessages(idAfterLast, null);
		assertEquals(rows.size(), 1);

		rows = db.getMessages(idAfterLast, "user123");
		assertEquals(rows.size(), 1);

		assertFalse(db.isHasUser("user11"));
		db.changeUser("user1", "user11");
		assertFalse(db.isHasUser("user1"));
		assertTrue(db.isHasUser("user11"));
		assertEquals(db.getUserCount(), 1);

		db.addIfNotExist("user2");
		assertEquals(db.getUserCount(), 2);
		assertTrue(db.isHasUser("user2"));
		assertTrue(db.isHasUser("user11"));
		db.addMessage(new MessageRow("user2", "message100"));
		rows = db.getMessages(null, "user1");
		assertEquals(rows.size(), 1);
		assertEquals(rows.get(0).getUser(), "user2");
		assertEquals(rows.get(0).getMessage(), "message100");

	}

	@Test
	public void testTiming() throws Throwable
	{
		IServerDB db = createDB(MAX_MESAGES);
		db.addIfNotExist("user1");
		db.addIfNotExist("user2");

		assertEquals(db.getUserCount(), 2);

		Thread.sleep(2000);
		
		db.removeOlderUser(10);
		assertEquals(db.getUserCount(), 2);
		
		db.removeOlderUser(1);
		assertEquals(db.getUserCount(), 0);
	}
	
	@Test
	public void testPing() throws Throwable
	{
		IServerDB db = createDB(MAX_MESAGES);
		db.addIfNotExist("user1");
		db.addIfNotExist("user2");

		assertEquals(db.getUserCount(), 2);

		Thread.sleep(2000);
		
		db.ping("user2");
		
		db.removeOlderUser(1);
		assertEquals(db.getUserCount(), 1);
		assertTrue(db.isHasUser("user2"));
		assertFalse(db.isHasUser("user1"));
	}

}
